const menuIcon = document.querySelector('.icon-menu-toggle');
const sideNav = document.querySelector('.side-nav');
const iconClose = document.querySelector('.icon-close-menu');

const days = document.querySelectorAll('.day');
const imgDays = document.querySelectorAll('.img-day');

const iconEnviroment = document.querySelectorAll('.work-enviroment .icon');
const contentEnviroment = document.querySelectorAll('.content-enviroment .img');

const menuShow = document.querySelector('#menu');
const menuBtn = document.querySelector('.menu-btn');
var menuOpen = false;

const phoneClick = document.querySelector('.smart-phone');
const imgSmartPhone = document.querySelector('.slide-top-2')

phoneClick.addEventListener('click', turnOffLight)


function turnOffLight(){
  if(imgSmartPhone.classList.contains('avtive-smart-phone'))
    imgSmartPhone.classList.remove('avtive-smart-phone');
  else
    imgSmartPhone.classList.add('avtive-smart-phone')
}
//header

$(window).on("scroll", function() {
  if($(window).scrollTop() > 50) {
      $(".header-navigation").addClass("bg-header");
  } else {
     $(".header-navigation").removeClass("bg-header");
  }
});


menuBtn.addEventListener('click', () => {
  if(!menuOpen) {
    menuBtn.classList.add('open');
    menuShow.classList.add('toggle-menu');
    menuOpen = true;
  } else {
    menuBtn.classList.remove('open');
    menuShow.classList.remove('toggle-menu');
    menuOpen = false;
  }
});

//slideshows
var swiper = new Swiper('#feature .swiper-container', {
  spaceBetween: 0,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  loop: true,
  // autoplay: {
  //   delay: 4500,
  //   disableOnInteraction: false,
  // }
});


var swiper = new Swiper('#smart-life .swiper-container', {
  slidesPerView: 1,
  spaceBetween: 0,
  loop: true,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});


var swiper = new Swiper('#lights .swiper-container', {
  spaceBetween: 0,
  centeredSlides: true,
  mousewheel: {
    releaseOnEdges: true,
  }
});
//

$(document).ready(function () {
  // Assign some jquery elements we'll need
  var $swiper = $(".swiper-container");
  var $bottomSlide = null; // Slide whose content gets 'extracted' and placed
  // into a fixed position for animation purposes
  var $bottomSlideContent = null; // Slide content that gets passed between the
  // panning slide stack and the position 'behind'
  // the stack, needed for correct animation style

  new Swiper("#make-in .swiper-container", {
    spaceBetween: 0,
    slidesPerView: 3,
    centeredSlides: true,
    roundLengths: true,
    loop: true,
    loopAdditionalSlides: 30,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    breakpoints: {
      0: {
        centeredSlides: false,

      },
      320: {
        slidesPerView: 1,
        spaceBetween: 0,
      },
      640: {
        slidesPerView: 3,
        spaceBetween: 0,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 40,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 0,
      },
    }
  });
});


//Video
$("body").on("click", "video", function () {
  if (this.controls) {
    return;
  }
  this.controls = true;
  this.play();

  $(this)
    .siblings("button")
    .hide();
});


//tab content 
for (let index = 0; index < days.length; index++) {
  days[index].addEventListener('click', () => {
    let _id = days[index].dataset.hinh;
    let imgDay = document.getElementById(_id);
    for (let i = 0; i < imgDays.length; i++) {
      imgDays[i].style.display = 'none';
    }
    imgDay.style.display = 'block';
  })
}



//

for (let i = 0; i < iconEnviroment.length; i++) {
  iconEnviroment[i].addEventListener('click', () => {
    console.log(iconEnviroment[i].children[0].children[0].src);
    let _idIcon = iconEnviroment[i].dataset.icon;
    let imgEnviroment = document.getElementById(_idIcon);
    for (let j = 0; j < contentEnviroment.length; j++) {
      contentEnviroment[j].style.display = "none";

    }
    imgEnviroment.style.display = "block";
  })

}
// function showenviroment(_id) {
//   for (let i = 0; i < contentEnviroment.length; i++) {
//     contentEnviroment[i].style.display = 'none';
//   }
//   var imgEn = document.getElementById(_id);
//   imgEn.style.display = 'block';
// }
// for (let i = 0; i < iconEnviroment.length; i++) {
//   iconEnviroment[i].addEventListener('click', () => {
//     var _id = this.innerHTML;
//     showenviroment(_id);
//   })
// }
// showenviroment('concentrate');




  // gsap.fromTo(".img-slide", {x: 0} , { duration: 5, x:300 });



